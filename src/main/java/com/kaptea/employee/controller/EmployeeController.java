package com.kaptea.employee.controller;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kaptea.employee.entity.Employee;
import com.kaptea.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class EmployeeController {

    private final EmployeeService employeeService;

    @Autowired
    EmployeeController(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    // From controller im hiting employees url
    // Fetching list of employees
    @GetMapping("/employees")
    public String employees(Model model, Employee employee) {

        List<Employee> employees = employeeService.findAll();
        model.addAttribute("employees", employees);

        return "employee"; // Name of my html file
    }

    // Explaining create first
    @PostMapping("/create-new")
    public String createNewEmployee(@Valid Employee employee, BindingResult result, Model model) {

        employeeService.save(employee);

        List<Employee> employees = employeeService.findAll();
        model.addAttribute("employees", employees);

        return "employee";
    }

    // This is my endpoint where i have written logic for the same

    @RequestMapping(value = "/employee-pdf-export", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> newsReport() throws IOException, DocumentException, ParseException {

        // Fetching list of added employees from the database
        List<Employee> employees = (List<Employee>) employeeService.findAll();

        // Written seperate method
        ByteArrayInputStream byteArrayInputStream = employeeGeneratePdf(employees);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=employee.pdf");
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Document document = new Document(PageSize.A4, 20, 20, 50, 25);
         PdfWriter.getInstance(document, bos);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(byteArrayInputStream));
    }


    public ByteArrayInputStream employeeGeneratePdf(List<Employee> employees) {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {

            // For font style
            Font headFont = FontFactory.getFont(FontFactory.COURIER_BOLDOBLIQUE, 10);
            List<String> headers = Arrays.asList("No", "First Name", "Middle Name", "Last Name", "Employee Code");
            PdfPTable table = new PdfPTable(headers.size());
            table.setWidthPercentage(90);
            table.setWidths(new int[]{1, 3, 3, 2, 3});

            // Setting headers
            for (String string : headers) {
                PdfPCell hcell;
                hcell = new PdfPCell(new Phrase(string, headFont));
                hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
                hcell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                table.addCell(hcell);
            }

            int count = 0;

            // Adding employees dynamically
            for (Employee employee : employees) {
                count ++ ;

                // For SR NO
                Font font = FontFactory.getFont(FontFactory.COURIER, 8);
                PdfPCell cell;
                cell = new PdfPCell(new Phrase(String.valueOf(count), font));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                // For first name
                cell = new PdfPCell(new Phrase(employee.getFirstName(), font));
                cell.setPaddingLeft(2);
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(employee.getMiddleName()), font));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setPaddingRight(2);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(employee.getLastName()), font));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setPaddingRight(2);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(employee.getEmployeeCode()), font));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setPaddingRight(2);
                table.addCell(cell);
            }

            PdfWriter.getInstance(document, out);
            document.open();
            Paragraph para = new Paragraph();
            Font fontHeader = new Font(Font.FontFamily.TIMES_ROMAN, 14, Font.BOLD);
            Font font1 = new Font(Font.FontFamily.COURIER, 10, Font.BOLD);

            // If you want to add logo to pdf
//            Image logo = Image.getInstance("");
//            logo.scaleAbsolute(50f, 50f);
//            logo.scaleToFit(80f, 80f);
//            logo.setAbsolutePosition(110, 755);
//            document.add(logo);
            para = new Paragraph("Kaptea Tutorials", fontHeader);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);
            para = new Paragraph("Bangalore", font1);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);

            para = new Paragraph("Active employees", fontHeader);
            para.setAlignment(Element.ALIGN_CENTER);
            document.add(para);

            para = new Paragraph(" ", fontHeader);
            document.add(para);
            document.add(table);
            document.close();
        } catch (DocumentException ex) {
            Logger.getLogger("PDF Download").log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(out.toByteArray());
    }


}
