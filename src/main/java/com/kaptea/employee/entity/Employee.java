package com.kaptea.employee.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name="employee")
public class Employee implements Serializable{

	private static final long serialVersionUID = -3781887609609178736L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

	@Column(length=25)
	private String employeeCode;

	@Column(length=25)
	private String firstName;

	@Column(length=25)
	private String middleName;

	@Column(length=25)
	private String lastName;

	@Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern ="yyyy-MM-dd")
	private Date dateOfJoining;

	@LazyCollection(LazyCollectionOption.TRUE)
	@OneToMany(cascade = CascadeType.ALL,mappedBy = "employee")
	private List<Qualification> qualification;

	private String username;

	private String password;

	private boolean active;

	public Employee(){}

}